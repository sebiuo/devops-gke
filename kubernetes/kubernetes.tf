terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      #version = "3.52.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      #version = ">= 2.0.1"
    }
  }
}

data "terraform_remote_state" "gke" {
  backend = "local"

  config = {
    path = "/Users/sebiuo/Documents/devops_prueba/GKE-TERRAFORM/terraform.tfstate"
  }
}

# Retrieve GKE cluster information
provider "google" {
  project = data.terraform_remote_state.gke.outputs.project_id
  region  = data.terraform_remote_state.gke.outputs.region
}

# Configure kubernetes provider with Oauth2 access token.
# https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/client_config
# This fetches a new token, which will expire in 1 hour.
data "google_client_config" "default" {}

data "google_container_cluster" "my_cluster" {
  name     = data.terraform_remote_state.gke.outputs.kubernetes_cluster_name
  location = data.terraform_remote_state.gke.outputs.region
}

provider "kubernetes" {
  host = data.terraform_remote_state.gke.outputs.kubernetes_cluster_host

  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(data.google_container_cluster.my_cluster.master_auth[0].cluster_ca_certificate)
}


# Ejemplo de SEBAPI

resource "kubernetes_deployment" "sebapi" {
  metadata {
    name = "seb-api"
    labels = {
      App = "SEBAPI"
    }
  }

  spec {
    replicas = 10
    selector {
      match_labels = {
        App = "SEBAPI"
      }
    }
    template {
      metadata {
        labels = {
          App = "SEBAPI"
        }
      }
      spec {
        container {
          image = "sebiuo/sebapi:latest"
          name  = "sebapi"

          port {
            container_port = 5000
          }

          resources {
            # Describes the maximum amount of compute resources allowed
            limits = {
              cpu    = "0.5"
              memory = "512Mi"
            }
            # Describes the minimum amount of compute resources required.
            requests = {
              cpu    = "250m"
              memory = "300Mi"
              ephemeral-storage: "3Gi"
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "sebapp" {
  metadata {
    name = "sebapp-service"
  }
  spec {
    selector = {
      App = kubernetes_deployment.sebapi.spec.0.template.0.metadata[0].labels.App
    }
    port {
      port        = 5000
      target_port = 5000
    }

    type = "LoadBalancer"
  }
}
