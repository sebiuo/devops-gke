# Se crea una cuenta de servicio en el proyecto y se le asignan permisos solo
# para utilizar kubernetes

resource "google_service_account" "service_account" {
  account_id   = "sebapi-id"
  display_name = "sebano"
  project = local.project_id 
}

resource "google_project_iam_member" "project" {
  project = local.project_id
  role    = "roles/container.admin"
  member  = "serviceAccount:${google_service_account.service_account.email}"
}

# module "my-app-workload-identity" {
#   source     = "terraform-google-modules/kubernetes-engine/google//modules/workload-identity"
#   name       = "k8s-limit-user"
#   namespace  = "development"
#   project_id = "host-123456"
#   roles      = ["roles/container.admin"]
# }
# 
# # Cuenta owner jijiji
# resource "google_service_account" "service_account1" {
#   account_id   = "sebowner-id"
#   display_name = "sebowner"
#   project = local.project_id 
# }

# resource "google_project_iam_member" "project1" {
#   project = local.project_id
#   role    = "roles/owner"
#   member  = "serviceAccount:${google_service_account.service_account.email}"
# }


