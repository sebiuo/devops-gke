# Control de version de TERRAFORM en GCP
Provisiona la infrastructura en GKE

Terraform ayuda a desplegar de forma declarativa la infrastructura desplegada,
controla el estado actual del cluster, y es facilmente leible su estrctura.

### 0- Pipeline
Para el control de versiones del cluster GKE se utiliza el pipeline que en primera instancia se conecta al servidor configurado para utilizar terraform y gcloud. Luego testea que los archivos cumplan el formato correcto. Por ultimo se actualiza el estado con los cambios realizados en la confuguración del cluster.

### 1- Es necesario tener activado en GCP:
    - API kubernetes
    - API IAM
    - Estar autentificado en GCP

### 2- Terraform
Se utiliza terraform para desplegar el cluster de kubernetes en Google Cloud Platform

### 3- VPC 
Se configura la Virtual Private Cloud, y su subred en el archivo vpc.tf

### 4- GKE
Luego se crea el cluster con los parametros definidos en el archivo gke.tf

### 5 LOCALS
Se usa el archivo locals.tf como locals donde se encuentra la variable id de proyecto y la region de despliegue.

### 6 Despliegue
Para desplegar el cluster:

En el servidor preparado dentro de la carpeta contenedora de los archivos se inicia terraform.
```
terraform init
```
Luego se valida el formato
```
terraform format
```
Se aplican los cambios y se aprueba el estado.
```
terraform apply
```
Para aceptar los cambios se debe escribir yes.

Se crea la configuració de kubectl utilizando los outputs generados.
```
gcloud container clusters get-credentials $(terraform output -raw kubernetes_cluster_name) --region $(terraform output -raw region)
```
